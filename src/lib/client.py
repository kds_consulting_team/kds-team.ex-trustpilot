import requests
# import sys
from kbc.client_base import HttpClientBase

BASE_URL = 'https://api.trustpilot.com/v1'


class TrustpilotClient(HttpClientBase):

    def __init__(self, api_token):

        _header = {'apikey': api_token}

        HttpClientBase.__init__(self, base_url=BASE_URL, default_http_header=_header)
        self.get_raw = requests.get

    def _find_business_unit(self, businessUnitName):

        _url = self.base_url + '/business-units/find'
        _params = {'name': businessUnitName}

        _rsp = self.get_raw(_url, _params, headers=self._auth_header)
        return _rsp.status_code, _rsp.json()

    def get_profile_info(self, businessUnitId):

        _url = self.base_url + f'/business-units/{businessUnitId}/profileinfo'

        _rsp = self.get_raw(_url, headers=self._auth_header)
        return _rsp.status_code, _rsp.json()

    def get_business_unit(self, businessUnitId):

        _url = self.base_url + f'/business-units/{businessUnitId}'

        _rsp = self.get_raw(_url, headers=self._auth_header)
        return _rsp.status_code, _rsp.json()

    def _get_business_unit_reviews(self, businessUnitId, page, perPage):

        _params = {'perPage': perPage,
                   'orderBy': 'createdat.desc',
                   'page': page}

        _url = self.base_url + f'/business-units/{businessUnitId}/reviews'
        _rsp = self.get_raw(_url, headers=self._auth_header, params=_params)

        return _rsp.status_code, _rsp.json()

    def get_paged_business_unit_reviews(self, businessUnitId):

        perPage = 100
        page = 0
        all_reviews = []

        while perPage == 100:

            page += 1
            _, _js = self._get_business_unit_reviews(businessUnitId, page, perPage)

            _reviews = _js['reviews']
            all_reviews += _reviews
            perPage = len(_reviews)

        return all_reviews
