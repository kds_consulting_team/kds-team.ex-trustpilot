import csv
import json
import logging
import os
import sys
from lib.client import TrustpilotClient
from kbc.env_handler import KBCEnvHandler


KEY_API_KEY = '#apiKey'
KEY_BUSINESS_UNITS = 'businessUnits'

MANDATORY_PARAMS = [KEY_API_KEY, KEY_BUSINESS_UNITS]

_FIELDS_BUSINESSUNIT_STATISTICS = ['id', 'displayName', 'name_identifying', 'websiteUrl',
                                   'trustScore', 'stars', 'country', 'status', 'numberOfReviews_total',
                                   'numberOfReviews_usedForTrustScoreCalculation', 'numberOfReviews_oneStar',
                                   'numberOfReviews_twoStars', 'numberOfReviews_threeStars',
                                   'numberOfReviews_fourStars', 'numberOfReviews_fiveStars']

_FIELDS_REVIEWS_DETAILS = ['id', 'consumer_id', 'consumer_displayName', 'consumer_displayLocation',
                           'consumer_numberOfReviews', 'businessUnit_id', 'location', 'stars',
                           'title', 'text', 'language', 'createdAt', 'updatedAt', 'companyReply',
                           'isVerified', 'numberOfLikes', 'status', 'countsTowardsTrustScore',
                           'countsTowardsLocationTrustScore']

_FIELDS_BUSINESSUNIT_PROFILE = ['businessUnitId', 'isSubscriber', 'email', 'phone', 'companyName',
                                'address_street', 'address_city', 'address_postcode', 'address_country',
                                'address_countryCode', 'description_header', 'description_text',
                                'socialmedia_facebook', 'socialmedia_linkedin', 'socialmedia_twitter',
                                'socialmedia_googleplus', 'socialmedia_youtube', 'socialmedia_instagram',
                                'facebookPageId', 'facebookPageUrl', 'isFacebookActivated', 'isCommentsEnabled',
                                'isIncentivisingUsers', 'isClaimed']


class Compoment(KBCEnvHandler):

    def __init__(self):

        KBCEnvHandler.__init__(self, MANDATORY_PARAMS)

        try:

            self.validate_config(mandatory_params=MANDATORY_PARAMS)

        except ValueError as e:

            logging.error(e)
            sys.exit(1)

        self.client = TrustpilotClient(self.cfg_params[KEY_API_KEY])

        _units = self.cfg_params[KEY_BUSINESS_UNITS]
        _units_list = [item.strip() for sublist in [bu.split(',')
                                                    for bu in _units.split('\n')] for item in sublist]

        try:
            _units_list.remove('')
        except ValueError:
            pass

        self.businessUnits = _units_list

        _stats_path = os.path.join(self.tables_out_path, 'business-units-statistics.csv')
        _reviews_path = os.path.join(self.tables_out_path, 'reviews.csv')
        _profiles_path = os.path.join(self.tables_out_path, 'business-units.csv')

        self.stats_writer = csv.DictWriter(open(_stats_path, 'w'), fieldnames=_FIELDS_BUSINESSUNIT_STATISTICS,
                                           restval='', extrasaction='ignore', quoting=csv.QUOTE_ALL)

        self.reviews_writer = csv.DictWriter(open(_reviews_path, 'w'), fieldnames=_FIELDS_REVIEWS_DETAILS,
                                             restval='', extrasaction='ignore', quoting=csv.QUOTE_ALL)

        self.profile_writer = csv.DictWriter(open(_profiles_path, 'w'), fieldnames=_FIELDS_BUSINESSUNIT_PROFILE,
                                             restval='', extrasaction='ignore', quoting=csv.QUOTE_ALL)

        self.stats_writer.writeheader()
        self.reviews_writer.writeheader()
        self.profile_writer.writeheader()

        self._create_manifest(_stats_path, PK=['id'])
        self._create_manifest(_profiles_path, PK=['businessUnitId'])
        self._create_manifest(_reviews_path, PK=['id', 'consumer_id', 'businessUnit_id'])

    def _create_manifest(self, path, incremental=True, PK=[], destination=''):

        _path = path + '.manifest'

        _manifest = {'incremental': incremental,
                     'destination': destination,
                     'primary_key': PK}

        with open(_path, 'w') as f:

            json.dump(_manifest, f)

    def find_business_unit(self, businessUnit):

        _sc, _js = self.client._find_business_unit(businessUnit)

        if _sc == 200:

            return _js['id']

        elif _sc == 401:

            logging.error(
                "Could not authenticate the request. Please check the API token.")
            sys.exit(1)

        elif _sc == 404:

            _sc_bu, _js_bu = self.client.get_profile_info(businessUnit)

            if _sc_bu == 200:

                return businessUnit

            else:

                logging.warn(
                    "Could not find business unit %s. Please check the configuration." % businessUnit)

    def _flatten_json(self, x, out={}, name=''):
        if type(x) is dict:
            for a in x:
                self._flatten_json(x[a], out, name + a + '_')
        else:
            out[name[:-1]] = x

        return out

    def get_and_write_statistics(self, businessUnitId):

        _sc, _js = self.client.get_business_unit(businessUnitId)

        if _sc != 200:

            logging.error(
                "There was an error downloading data for unit %s." % businessUnitId)
            sys.exit(1)

        del _js['links']
        self.stats_writer.writerow(self._flatten_json(_js))

    def get_and_write_profile_info(self, businessUnitId):

        _sc, _js = self.client.get_profile_info(businessUnitId)

        if _sc != 200:

            logging.error(
                "There was an error downloading profile info for unit %s." % businessUnitId)
            sys.exit(1)

        del _js['links']
        _profile = self._flatten_json(_js)
        _profile_sanitized = self._sanitize_columns(_profile)

        _to_write = {**{'businessUnitId': businessUnitId},
                     **_profile_sanitized}

        self.profile_writer.writerow(_to_write)

    @staticmethod
    def _sanitize_columns(dictionary):

        for key in dictionary:

            value = dictionary[key]
            dictionary[key] = str(value).replace('\n', '').replace('\r', '')

        return dictionary

    def get_and_write_reviews(self, businessUnitId):

        reviews = self.client.get_paged_business_unit_reviews(businessUnitId)

        for r in reviews:

            _flattened = self._flatten_json(r)
            del _flattened['links']
            del _flattened['consumer_links']
            del _flattened['businessUnit_links']

            _sanitized = self._sanitize_columns(_flattened)
            self.reviews_writer.writerow(_sanitized)

    def run(self):

        for bu in self.businessUnits:

            businessUnitId = self.find_business_unit(bu)
            if businessUnitId is None:

                continue

            logging.info(
                "Extracting business unit information for %s." % businessUnitId)
            self.get_and_write_statistics(businessUnitId)

            logging.info(
                "Extracting profile information for unit %s." % businessUnitId)
            self.get_and_write_profile_info(businessUnitId)

            logging.info("Extracting reviews for unit %s." % businessUnitId)
            self.get_and_write_reviews(businessUnitId)
