# TrustPilot extractor

The TrustPilot extractor allows users to extract reviews for the specified business units. All that is needed is the API key and either IDs or names of the business units for which the reviews should be downloaded. Besides reviews, profile information and statistics are downloaded as well.

All tables are loaded incrementally to storage.

## Output

The output of the extractor are 3 tables: **business-units**. **business-units-statistics** and **reviews**. All of the tables are loaded incrementally and each has a unique primary key.

The sample of output tables can be found [here](https://bitbucket.org/kds_consulting_team/kds-team.ex-trustpilot/src/master/component_config/sample-config/out/tables/). 