The sample configuration can be found in the [components' repository](https://bitbucket.org/kds_consulting_team/kds-team.ex-trustpilot/src/master/component_config/sample-config/config.json).

### API Key (`#apiKey`)

The API Key is the main form of authentication for TrustPilot. If the wrong key is provided, the application fails due to not being authorized to download anything. Follow (this article) to learn, how to obtain the API key.

### Business Units (`businessUnits`)

The comma or new line separated list of business units, for which the reviews should be downloaded. If a business unit does not exist in TrustPilot database, no reviews are extracted for it. Follow [this article](https://developers.trustpilot.com/tutorials/how-to-find-your-business-unit-id) to learn, how to find the business unit identificator.

## Output

The output of the extractor are 3 tables: **business-units**. **business-units-statistics** and **reviews**. All of the tables are loaded incrementally and each has a unique primary key.

The sample of output tables can be found [here](https://bitbucket.org/kds_consulting_team/kds-team.ex-trustpilot/src/master/component_config/sample-config/out/tables/). 