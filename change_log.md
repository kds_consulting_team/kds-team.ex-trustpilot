**0.1.0**
Public version of the component.
Added descriptions to configurations, sample configurations and outputs.

**0.0.2**
Added manifests creation for tables

**0.0.1**
First working version of the component.